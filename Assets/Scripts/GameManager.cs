﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    [Header("Score UI")]
    public Text T_score;
    public Text T_HighScore;

    int Score = 0;
    int HighScore = 0;

    // Start is called before the first frame update
    void Start()
    {
        HighScore = PlayerPrefs.GetInt("HighScore");
    }

    // Update is called once per frame
    void Update()
    {
        T_score.text = "Score: " + Score.ToString();
        T_HighScore.text = "High Score: " + HighScore.ToString();
    }

    private void OnTriggerEnter(Collider other)
    {
        switch (other.gameObject.tag){
            case "collectable":

                break;
            case "multiplier":

                break;
            case "minus":

                break;
        }
    }

    public void AddToScore()
    {
        Score += 1;
        if(Score>= HighScore)
        {
            HighScore = Score;
            PlayerPrefs.SetInt("HighScore", HighScore);
            PlayerPrefs.Save();
        }

    }


}
