﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Torch : MonoBehaviour
{
    public GameObject torch; //The torch
    public Slider BatteryLevelBar; //UI for battery level
    float batteryPower = 20f; //Current battery level
    float maxBatteryCapacity = 100f; //how much can we store in one go?

    float torchIntesity; //To store orignal starting intensity value


    // Start is called before the first frame update
    void Start()
    {
        torchIntesity = torch.GetComponent<Light>().intensity; //set orignial value
    }

    // Update is called once per frame
    void Update()
    {
        //Toggle torch active
        if(Input.GetKeyDown(KeyCode.T))
        {
            torch.SetActive(!torch.activeSelf); //if true, set to false. if false, set to true
        }

        //Take away battery if on
        if(torch.activeSelf)
        {
            //Take away 1 every seccond if above 0
            if (batteryPower > 0) 
            { 
                batteryPower -= 1f * Time.deltaTime; 
            }

            if(batteryPower < 10f) //Less than 10?
            {
                //Dim the torch if battery is less than 10. 
                /** Maths Explained...
                We need to convert the batteryPower which will be between 0-10 to a
                value of 0-1 that we can use as division by multiplication of the original brightness. 
                    eg if we have 10 battery power, divide by 10 = 1... original torchIntensity * 1 = torchintnsity. 
                    eg if we have 5 battery power, divide by 10 = 0.5... original torchIntensity * 0.5 = torchintenisty/5
                 **/
                torch.GetComponent<Light>().intensity = torchIntesity * batteryPower / 10f;
            }   
        }
        BatteryLevelBar.value = batteryPower;
    }

    public void AddBatteryPower(float value)
    {
        //Add the value of the new battery
        batteryPower += value;
        //Check if we have gone over max capacity
        if(batteryPower > maxBatteryCapacity)
        {
            //if so, set to max.
            batteryPower = maxBatteryCapacity;
        }
        //Incase we were low, set intensity back up to original value
        torch.GetComponent<Light>().intensity = torchIntesity;
    }
}
