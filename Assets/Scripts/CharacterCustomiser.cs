﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterCustomiser : MonoBehaviour
{
    public MouseLook mouseLook; //Our MouseLook script attached to the player's camera
    public RigidbodyFPSController player;
    public Transform HatTransform; //The spawn position for our hat
    public MeshRenderer BodyMesh;

    GameObject CurrentHat; //The spawned Hat GameObject

    [Header("Hats and Colours Arrays")]
    public GameObject[] Hats; //Array of Hats
    public Color[] BodyColours; //Array of Colours

    int CurrentHatIndex = 0; //Index for current hat
    int CurrentColourIndex = 0; //Index for current colour

    [Header("UI")]
    public GameObject MenuPanel;
    public GameObject[] HatOptions;
    public GameObject[] ColourOptions;

    // Start is called before the first frame update
    void Start()
    {
        //Set the Hat and Colour from last time the player played
        SetHat(PlayerPrefs.GetInt("Hat"));
        SetColour(PlayerPrefs.GetInt("BodyColor"));
    }

    // Update is called once per frame
    void Update()
    {
        //M for 'Menu'
        if(Input.GetKeyDown(KeyCode.M))
        {
            ToggleMenu();
            HatOptions[0].GetComponentInParent<Button>().Select();
        }
        if(Input.GetKeyDown(KeyCode.H))
        {
            //Check how many hats we have to determine if we should loop back to 0
            if(CurrentHatIndex < Hats.Length -1)
                SetHat(CurrentHatIndex +1); //Increment to the next hat
            else
                SetHat(0); //Loop back to first hat
        }
        //Check how many colours we have to determine if we should loop back to 0
        if (Input.GetKeyDown(KeyCode.C))
        {
            if (CurrentColourIndex < BodyColours.Length - 1)
                SetColour(CurrentColourIndex + 1); //inrement to the next colour
            else
                SetColour(0);//Loop back to first colour
        }
    }

    void ToggleMenu()
    {
        MenuPanel.SetActive(!MenuPanel.activeSelf); //Toggle the UI on and off

        //Toggle cursor lockstate on and off
        if (Cursor.lockState != CursorLockMode.None) //Is lockstate not 'None'?
            Cursor.lockState = CursorLockMode.None;//if so, set it to 'None'
        else
            Cursor.lockState = CursorLockMode.Locked; //Otherwise set to locked

        mouseLook.enabled = !mouseLook.enabled; //Toggle the mouselook script on and off

        player.AcceptInput = !player.AcceptInput; // Togglr player input from our character controller
    }

    public void SetHat(int selection)
    {
        HatOptions[CurrentHatIndex].SetActive(false); //change last option to off
        HatOptions[selection].SetActive(true); //Change selected option to on

        Destroy(CurrentHat); //Destroy the old hat GameObject 
        CurrentHatIndex = selection; //update our index to keep track of current hat
        //Spawn the chosen hat
        CurrentHat = Instantiate(Hats[selection], HatTransform.position, HatTransform.rotation, HatTransform); 
        //Update PlayerPrefs
        PlayerPrefs.SetInt("Hat", selection);
        PlayerPrefs.Save();
    }

    public void SetColour(int selection)
    {
        ColourOptions[CurrentColourIndex].SetActive(false); //change last option to off
        ColourOptions[selection].SetActive(true); //Change selected option to on

        CurrentColourIndex = selection; //update our index to keep track of current colour
        //Get the material from our player and change its colour.
        BodyMesh.material.color = BodyColours[selection]; 
        //Update PlayerPrefs
        PlayerPrefs.SetInt("BodyColor", selection);
        PlayerPrefs.Save();
    }

}
