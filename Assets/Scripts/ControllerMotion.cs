﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControllerMotion : MonoBehaviour
{
    //create a reference of our controlls script 
    Controlls controlls;

    //create 2 vector2's for the positions of our joysticks. 
    Vector2 LeftStick;
    Vector2 RightStick;

    //Create 2 vector2's for the Face Buttons 
    float B_North, B_East, B_South, B_West;

    public float JoyStickIntensity = 2f;
    public float FaceButtonIntensity = 1f;

    //Create 2 public GameObject references to be assigned in the scene. 
    public GameObject G_LeftStick;
    public GameObject G_RightStick;

    //Create 4 GameObject references of our face buttons
    public GameObject G_North, G_East, G_South, G_West;

    Vector3 NorthPos, EastPos, SouthPos, WestPos;


    //Called before the first frame is rendered. 
    void Awake()
    {
        //Create an instance of our controlls script to our reference
        controlls = new Controlls();

        //Add actions to the events within our controlls script
        //--if left joy stick was moved, create an event called 'ctx' and add the action of
        //--setting our 'LeftStick' vector 2 to the value of the controller. This will now 
        //--happen every time the controllers joy stick was moved. 
        controlls.Player.LeftJoystick.performed += ctx => LeftStick = ctx.ReadValue<Vector2>();
        controlls.Player.LeftJoystick.canceled += ctx => LeftStick = Vector2.zero;
        //Do same for the Right Stick...
        controlls.Player.RightJoystick.performed += ctx => RightStick = ctx.ReadValue<Vector2>();
        controlls.Player.RightJoystick.canceled += ctx => RightStick = Vector2.zero;

        //Add actions to the face buttons of our controller
        controlls.Player.ButtonNorth.performed += ctx => B_North = ctx.ReadValue<float>();
        controlls.Player.ButtonEast.performed += ctx => B_East = ctx.ReadValue<float>();
        controlls.Player.ButtonSouth.performed += ctx => B_South = ctx.ReadValue<float>();
        controlls.Player.ButtonWest.performed += ctx => B_West = ctx.ReadValue<float>();
        //and for release, set to 0.
        controlls.Player.ButtonNorth.canceled += ctx => B_North = 0f;
        controlls.Player.ButtonEast.canceled += ctx => B_East = 0f;
        controlls.Player.ButtonSouth.canceled += ctx => B_South = 0f;
        controlls.Player.ButtonWest.canceled += ctx => B_West = 0f;

    }

    private void Start()
    {
        //Get starting position of each face button. 
        NorthPos = G_North.transform.position;
        EastPos = G_East.transform.position;
        SouthPos = G_South.transform.position;
        WestPos = G_West.transform.position;
    }

    //Called when this scipt is enabled in the scene
    private void OnEnable()
    {
        controlls.Enable();
    }

    //Called whem this gameobject is disabled from the scene
    private void OnDisable()
    {
        controlls.Disable();
    }

    // Update is called once per frame
    void Update()
    {
        //Access the transform component of our G_LeftStick GameObject and
        //set the rotation to the values of our LeftStick vector2. Times
        //this value by Intensity to scale up the effect.
        G_LeftStick.transform.rotation = Quaternion.Euler(
            LeftStick.y * JoyStickIntensity,
            0f,
            -LeftStick.x * JoyStickIntensity);
        //Same but for G_RightStick
        G_RightStick.transform.rotation = Quaternion.Euler(
             RightStick.y * JoyStickIntensity,
             0f,
             -RightStick.x * JoyStickIntensity);


        //If face button is down, set to lower position
        if(B_North ==1f )
        {
            G_North.transform.position = NorthPos+ Vector3.down *FaceButtonIntensity;
        }
        else
        {
            G_North.transform.position = NorthPos;
        }
        if(B_East==1f)
        {
            G_East.transform.position = EastPos+ Vector3.down *FaceButtonIntensity;
        }
        else
        {
            G_East.transform.position = EastPos;
        }
        if (B_South==1f)
        {
            G_South.transform.position = SouthPos+  Vector3.down *FaceButtonIntensity;
        }
        else
        {
            G_South.transform.position = SouthPos;
        }
        if(B_West==1f)
        {
            G_West.transform.position = WestPos+ Vector3.down *FaceButtonIntensity;
        }
        else
        {
            G_West.transform.position = WestPos;
        }
    }


}
