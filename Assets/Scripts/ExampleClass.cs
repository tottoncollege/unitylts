﻿//Here we need to state what libraries we are going to need to use. 
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//We can add others, for example to access variables of UI elements (Text, Image, Sliders etc)
//we would need the following:
using UnityEngine.UI;

//or to be able to load scenes we need:
using UnityEngine.SceneManagement;

//This is our class. Everything to do with this script should be contained within
//the { below and } at the very bottom of the script. The name 'ExampleClass' below also needs
//to be the same as the file name for Unity to recognise it. MonoBehaviuor is Unity's system which
//allows scripts to manipulate GameObjects. It is this system which calls Unity Methods for us too. 
public class ExampleClass : MonoBehaviour
{
    //---------------------------------------------------------------------
    //-------------- Below are some example variable types ---------------- 
    //---------------------------------------------------------------------

        //public means editable within Unity
    public float floatValueUninitialised;
    public float floatValueInitialised = 23.4f;

        //by defualt, unless stated public, variables are private
    float floatValuePrivateUninitialised;
    private float floatValuePrivateInitialised = 2.4f;

    public float myFloat = 2.4f; //A decimal number which must be apended with an 'f'.
    public bool myBool; //A varialbe that can be only be either on/off, true/false, 1/0. Displays as a tick box in editor.
    public string myString = "This is the 'value' of mySting..."; //Must be in quotation marks.
    public int myInt = 25; //A whole number.
    public char myChar = 'a'; //A singal leter/character from the ASCII character table.

    //To organise our public variables we can add headers. These are purly decrotive. 
    [Header("This is a Heading")]
    public string sectionedString = "This varaible will be displayed under a header in the inspector";

    //---------------------------------------------------------------------
    //------------Below are the Unity called Methods in order ------------- 
    //---------------------------------------------------------------------

    private void Awake()
    {
        Debug.Log("Oh I sure do Hate mornings, but here I am!");
    }

    private void OnEnable()
    {
        
    }
    void Start()
    {
        //We can call our own functions from these to automate logic.
        //We just need to write the name of our method followed by '()' eg...
        MyStartMethod();
    }

    private void FixedUpdate()
    {
        
    }

    void Update()
    {
        MyUpdate();
    }

    private void LateUpdate()
    {
        
    }

    private void OnDisable()
    {
        
    }

    private void OnDestroy()
    {
        Debug.Log("Good night!");
    }

    //Collision/Trigger methods for 'Enter'. 
    //Exit & Stay also avaialbe for the four below...

    private void OnCollisionEnter(Collision collision)
    {
        
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        
    }
    private void OnTriggerEnter(Collider other)
    {
        
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        
    }

    //---------------------------------------------------------------------
    //---------------Below are some custom made Methods -------------------
    //---------------------------------------------------------------------

    void MyMethod1 ()
    {

    }

    void MyMethod2()
    {

    }

    void MyMethod3()
    {

    }

    //---------------------------------------------------------------------
    //----------Below are some custom made Methods with logic -------------
    //---------------------------------------------------------------------

    void MyStartMethod()
    {
        //We can use Debug.Log's to show more info of whats going on with our scripts as they happen
        Debug.Log("This is a debug message"); //displayed in white
        Debug.LogWarning("This is a debug warning"); //displayed in orange
        Debug.LogError("This is a debug error"); //displayed in red

    }
    void MyUpdate()
    {
        //We can easily create a timer by adding 'deltaTime' to a float variable
        //every frame (update). delta time is the time it took to update the last 
        //rendered frame. 
        myFloat += Time.deltaTime;

        //once myFloat's value is bigger than floatValueInitialised, set myBool to true. 
        if(myFloat > floatValueInitialised)
        {
            myBool = true;
        }
    }
}
