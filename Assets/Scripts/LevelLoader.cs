﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement; //needed to use scenemanager

public class LevelLoader : MonoBehaviour
{
    public enum loadType
    {
        Start,
        Timer,
        Collision,
        Trigger,
        UI
    }

    public loadType selectType;

    public string leveStringToLoad;

    [Header("Timer Load")]
    public float timeToLoad = 5f;
    float timer = 0f;

    [Header("Collision/Trigger Load")]
    public string collisionTagToCheck = "player";



    private void Start()
    {
        if(selectType == loadType.Start)
        {
            LoadLevel();
        }
    }

    private void Update()
    {
        if(selectType == loadType.Timer)
        {
            timer += Time.deltaTime;
            if(timer> timeToLoad)
            {
                LoadLevel();
            }
        }
    }

    /// <summary>
    /// This function can be called from this script, another script or a UI button. 
    /// </summary>
    public void LoadLevel ()
    {
        if (leveStringToLoad == "")
            Debug.LogWarning("No level string set on " + this.gameObject.name + "'s LevelLoader component!");
        else
            SceneManager.LoadScene(leveStringToLoad);
    }

    /// <summary>
    /// The following funtions are for collision/trigger detection only. If using collision, 
    /// this GameObject must also have a rigidbody.
    /// </summary>
    private void OnCollisionEnter(Collision collision)
    {
        if (selectType == loadType.Collision)
        {
            if(collision.gameObject.CompareTag(collisionTagToCheck))
            {
                LoadLevel();
            }
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (selectType == loadType.Collision)
        {
            if (collision.gameObject.CompareTag(collisionTagToCheck))
            {
                LoadLevel();
            }
        }
    }
    private void OnTriggerEnter(Collider collision)
    {
        if (selectType == loadType.Trigger)
        {
            if (collision.gameObject.CompareTag(collisionTagToCheck))
            {
                LoadLevel();
            }
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (selectType == loadType.Trigger)
        {
            if (collision.gameObject.CompareTag(collisionTagToCheck))
            {
                LoadLevel();
            }
        }
    }


}
