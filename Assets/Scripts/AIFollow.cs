﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AIFollow : MonoBehaviour
{

    public GameObject Player;
    NavMeshAgent navMeshAgent;
    public bool Freindly = false;
    public float FreindZone = 2f;
    public float AttackZone = 1f;
    float distance = 0f;

    private void Awake()
    {
        navMeshAgent = GetComponent<NavMeshAgent>();
    }
    void Start()
    {
        if (!Player)
        {
            Debug.Log("Make sure your player is tagged!!");
        }
    }
    void Update()
    {
        distance = Vector3.Distance(transform.position, Player.transform.position);
        if (Freindly) // We're going on an adventure!
        {
            if (distance > FreindZone)
            {
                navMeshAgent.isStopped = false;
                navMeshAgent.destination = Player.transform.position;
            }
            else
                navMeshAgent.isStopped = true;
        }
        else // Atackkkkk!
        {
            navMeshAgent.destination = Player.transform.position; // Always follow player pos

            if (distance < AttackZone)
            {
                //Handle Attack Here...
                Debug.Log("THATS A LOT OF DAMAGE");
            }
        }
    }


}
