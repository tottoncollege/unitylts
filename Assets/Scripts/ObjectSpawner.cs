﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectSpawner : MonoBehaviour
{
    [Header("Spawn Points (As many as you need)")]
    public Transform[] SpawnPoints;

    [Header("Objects to be spawned")]
    public GameObject[] Objects;

    [Header("Spawn Settings")]
    public bool Pause = false;
    public float Rate = 1f;
    

    //private variables
    private float SpawnTimer = 0f;


    private void Awake()
    {
        if(SpawnPoints.Length == 0)
        {
            Debug.LogError("Missing spawn points!");
            Pause = true;
        }
            
        if (Objects.Length == 0)
        {
            Debug.LogError("Missing Objects to be spawned!");
            Pause = true;
        }
            
    }

    // Update is called once per frame
    void Update()
    {
        if(!Pause)
        {
            SpawnTimer += Time.deltaTime;
            if(SpawnTimer>=Rate)
            {
                Spawn();
                SpawnTimer = 0f;
            }
        }
    }

    void Spawn()
    {
        int ObjRand = Random.Range(0, Objects.Length);
        int SpawnRand = Random.Range(0, SpawnPoints.Length);

        Instantiate(Objects[ObjRand], SpawnPoints[SpawnRand].position, SpawnPoints[SpawnRand].rotation);
    }
}
