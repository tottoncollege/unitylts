// GENERATED AUTOMATICALLY FROM 'Assets/Controlls.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public class @Controlls : IInputActionCollection, IDisposable
{
    public InputActionAsset asset { get; }
    public @Controlls()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""Controlls"",
    ""maps"": [
        {
            ""name"": ""Player"",
            ""id"": ""be080618-00c0-429b-80f9-334ee7fba784"",
            ""actions"": [
                {
                    ""name"": ""LeftJoystick"",
                    ""type"": ""Value"",
                    ""id"": ""d42548ee-7ef7-43bb-bcad-2b355d0c438d"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""RightJoystick"",
                    ""type"": ""Value"",
                    ""id"": ""61c37a78-642b-4afb-b5d7-ccdd48506082"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""ButtonSouth"",
                    ""type"": ""Button"",
                    ""id"": ""134fa421-565a-4954-bffa-65074796b8d6"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""ButtonNorth"",
                    ""type"": ""Button"",
                    ""id"": ""2da113dd-8cb0-45bc-b827-530bcb1e8194"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""ButtonWest"",
                    ""type"": ""Button"",
                    ""id"": ""88ea9f9e-c182-4be9-bb9f-8976e9009bcc"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""ButtonEast"",
                    ""type"": ""Button"",
                    ""id"": ""995a23d4-390c-4804-b808-31987c3b2a83"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""5e0dd625-d18f-4deb-81c0-2003edb2315b"",
                    ""path"": ""<Gamepad>/leftStick"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""LeftJoystick"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""2c5ea539-6479-43ad-9d71-1b9324cfb3bc"",
                    ""path"": ""<Gamepad>/rightStick"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""RightJoystick"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""3c019af9-d1cc-4603-a58d-d0fad80e669d"",
                    ""path"": ""<Gamepad>/buttonSouth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""ButtonSouth"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""6137afc0-d99c-4a3a-beb2-1609c5a69ebe"",
                    ""path"": ""<Gamepad>/buttonNorth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""ButtonNorth"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""3d6120ea-55e3-488a-9e18-285cd1695a0b"",
                    ""path"": ""<Gamepad>/buttonWest"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""ButtonWest"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""4775f2d8-0366-49e6-a1c8-6ca747e4f8df"",
                    ""path"": ""<Gamepad>/buttonEast"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""ButtonEast"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": []
}");
        // Player
        m_Player = asset.FindActionMap("Player", throwIfNotFound: true);
        m_Player_LeftJoystick = m_Player.FindAction("LeftJoystick", throwIfNotFound: true);
        m_Player_RightJoystick = m_Player.FindAction("RightJoystick", throwIfNotFound: true);
        m_Player_ButtonSouth = m_Player.FindAction("ButtonSouth", throwIfNotFound: true);
        m_Player_ButtonNorth = m_Player.FindAction("ButtonNorth", throwIfNotFound: true);
        m_Player_ButtonWest = m_Player.FindAction("ButtonWest", throwIfNotFound: true);
        m_Player_ButtonEast = m_Player.FindAction("ButtonEast", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }

    // Player
    private readonly InputActionMap m_Player;
    private IPlayerActions m_PlayerActionsCallbackInterface;
    private readonly InputAction m_Player_LeftJoystick;
    private readonly InputAction m_Player_RightJoystick;
    private readonly InputAction m_Player_ButtonSouth;
    private readonly InputAction m_Player_ButtonNorth;
    private readonly InputAction m_Player_ButtonWest;
    private readonly InputAction m_Player_ButtonEast;
    public struct PlayerActions
    {
        private @Controlls m_Wrapper;
        public PlayerActions(@Controlls wrapper) { m_Wrapper = wrapper; }
        public InputAction @LeftJoystick => m_Wrapper.m_Player_LeftJoystick;
        public InputAction @RightJoystick => m_Wrapper.m_Player_RightJoystick;
        public InputAction @ButtonSouth => m_Wrapper.m_Player_ButtonSouth;
        public InputAction @ButtonNorth => m_Wrapper.m_Player_ButtonNorth;
        public InputAction @ButtonWest => m_Wrapper.m_Player_ButtonWest;
        public InputAction @ButtonEast => m_Wrapper.m_Player_ButtonEast;
        public InputActionMap Get() { return m_Wrapper.m_Player; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(PlayerActions set) { return set.Get(); }
        public void SetCallbacks(IPlayerActions instance)
        {
            if (m_Wrapper.m_PlayerActionsCallbackInterface != null)
            {
                @LeftJoystick.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnLeftJoystick;
                @LeftJoystick.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnLeftJoystick;
                @LeftJoystick.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnLeftJoystick;
                @RightJoystick.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnRightJoystick;
                @RightJoystick.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnRightJoystick;
                @RightJoystick.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnRightJoystick;
                @ButtonSouth.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnButtonSouth;
                @ButtonSouth.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnButtonSouth;
                @ButtonSouth.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnButtonSouth;
                @ButtonNorth.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnButtonNorth;
                @ButtonNorth.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnButtonNorth;
                @ButtonNorth.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnButtonNorth;
                @ButtonWest.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnButtonWest;
                @ButtonWest.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnButtonWest;
                @ButtonWest.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnButtonWest;
                @ButtonEast.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnButtonEast;
                @ButtonEast.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnButtonEast;
                @ButtonEast.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnButtonEast;
            }
            m_Wrapper.m_PlayerActionsCallbackInterface = instance;
            if (instance != null)
            {
                @LeftJoystick.started += instance.OnLeftJoystick;
                @LeftJoystick.performed += instance.OnLeftJoystick;
                @LeftJoystick.canceled += instance.OnLeftJoystick;
                @RightJoystick.started += instance.OnRightJoystick;
                @RightJoystick.performed += instance.OnRightJoystick;
                @RightJoystick.canceled += instance.OnRightJoystick;
                @ButtonSouth.started += instance.OnButtonSouth;
                @ButtonSouth.performed += instance.OnButtonSouth;
                @ButtonSouth.canceled += instance.OnButtonSouth;
                @ButtonNorth.started += instance.OnButtonNorth;
                @ButtonNorth.performed += instance.OnButtonNorth;
                @ButtonNorth.canceled += instance.OnButtonNorth;
                @ButtonWest.started += instance.OnButtonWest;
                @ButtonWest.performed += instance.OnButtonWest;
                @ButtonWest.canceled += instance.OnButtonWest;
                @ButtonEast.started += instance.OnButtonEast;
                @ButtonEast.performed += instance.OnButtonEast;
                @ButtonEast.canceled += instance.OnButtonEast;
            }
        }
    }
    public PlayerActions @Player => new PlayerActions(this);
    public interface IPlayerActions
    {
        void OnLeftJoystick(InputAction.CallbackContext context);
        void OnRightJoystick(InputAction.CallbackContext context);
        void OnButtonSouth(InputAction.CallbackContext context);
        void OnButtonNorth(InputAction.CallbackContext context);
        void OnButtonWest(InputAction.CallbackContext context);
        void OnButtonEast(InputAction.CallbackContext context);
    }
}
